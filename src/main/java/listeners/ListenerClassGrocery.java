package listeners;
import com.aventstack.extentreports.ExtentReports;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import utils.GroceryUtils;

import java.io.File;
import java.io.IOException;

public class ListenerClassGrocery implements ITestListener {

    private GroceryUtils utils;
    public Logger logger = LogManager.getLogger(ListenerClassGrocery.class);
    private WebDriver driver;
    private ExtentReports extent;
    private String screenshotFolder = "./Screenshots/";

    public void onStart(ITestContext context) {
        extent = new ExtentReports();
        utils = new GroceryUtils();
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public void onTestSuccess(ITestResult result) {
        System.out.println("Grocery Test is Pass ");
    }

    public void onTestFailure(ITestResult result) {
        captureAndSaveScreenshot(result.getName());
    }

    private void captureAndSaveScreenshot(String testName) {
        TakesScreenshot ts = (TakesScreenshot) utils.getDriver();
        File source = ts.getScreenshotAs(OutputType.FILE);
        String destinationPath = screenshotFolder + testName + ".png";

        try {
            File destination = new File(destinationPath);
            org.apache.commons.io.FileUtils.copyFile(source, destination);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onTestSkipped(ITestResult result) {
        System.out.println("The Grocery Go Test has been Skipped ");
    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        System.out.println("The Grocery Go Test is within Success Percentage");
    }

    public void onTestFailedWithTimeout(ITestResult result) {
        System.out.println("The Grocery Go Test Failed due to time out.");
    }
    public void onFinish(ITestContext context) {
        extent.flush();
    }

}

