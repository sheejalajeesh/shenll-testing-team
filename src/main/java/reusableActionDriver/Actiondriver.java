package reusableActionDriver;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.function.Function;

import static utils.GroceryUtils.driver;

public class Actiondriver {

    public static void scrollByVisibilityOfElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("arguments[0].scrollIntoView();", ele);
    }

    public static void click(WebDriver ldriver,WebElement locatorName) {

        Actions act = new Actions(ldriver);
        act.moveToElement(locatorName).click().build().perform();
    }

    public static boolean findElement(WebDriver ldriver,WebElement ele) {
        boolean flag = false;
        try {
            ele.isDisplayed();
            flag = true;
        } catch (Exception e) {
            flag =false;
        }finally {
            if(flag) {
                System.out.println("Successfully found element at");
            }else {
                System.out.println("Unable to locate element at");
            }
        }
        return flag;

    }

    public static void setImplicitWait(WebDriver driver, int timeoutInSeconds) {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(timeoutInSeconds));

    }

    public static WebDriverWait setExplicitWait(WebDriver driver, int timeOutInSeconds) {
        return new WebDriverWait(driver, Duration.ofSeconds(timeOutInSeconds));
    }

    public static void setPageLoadTimeout(WebDriver driver, int timeoutInSeconds) {
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(30));
    }

    public static WebElement waitForElement(WebDriver driver,int timeOutInSeconds) {
        //Declare and initialise a fluent wait
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(Duration.ofSeconds(timeOutInSeconds))
                .pollingEvery(Duration.ofMillis(500))
                .ignoring(org.openqa.selenium.NoSuchElementException.class);

        return wait.until(new Function<WebDriver, WebElement>() {

            public WebElement apply(WebDriver t) {
                // TODO Auto-generated method stub
                return null;
            }


        });
    }

    public static boolean isDisplayed(WebDriver ldriver,WebElement ele) {
        boolean flag = false;
        flag = findElement(ldriver, ele);
        if (flag) {
            flag = ele.isDisplayed();
            if(flag) {
                System.out.println("The element is displayed");
            }else {
                System.out.println("The element is not displayed");
            }
        }else {
            System.out.println("Not displayed");
        }
        return flag;
    }

    public static boolean isSelected(WebDriver ldriver,WebElement ele) {
        boolean flag = false;
        flag = findElement(ldriver, ele);
        if (flag) {
            flag = ele.isSelected();
            if(flag) {
                System.out.println("The element is selected");
            }else {
                System.out.println("The element is not selected");
            }
        }else {
            System.out.println("Not selected");
        }
        return flag;
    }

    public static boolean isEnabled(WebDriver ldriver,WebElement ele) {
        boolean flag = false;
        flag = findElement(ldriver, ele);
        if (flag) {
            flag = ele.isEnabled();
            if(flag) {
                System.out.println("The element is Enabled");
            }else {
                System.out.println("The element is not Enabled");
            }
        }else {
            System.out.println("Not Enabled");
        }
        return flag;
    }

    public static void scrollDown(WebDriver driver, int scrollAmount) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        for (int i = 0; i < scrollAmount; i++) {
            js.executeScript("window.scrollBy(0, 250);");
        }
    }

    public static boolean type(WebElement ele,String text) {
        boolean flag = false;
        try {
            flag = ele.isDisplayed();
            //ele.clear();
            ele.sendKeys(text);
            flag =true;
        } catch (Exception e) {
            System.out.println("Location not found");
            flag = false;
        }finally {
            if(flag) {
                System.out.println("Successfully entered value");
            }else {
                System.out.println("Unable to enter value");
            }
        }
        return flag;
    }

    public static boolean selectbySendkeys(WebElement ele,String value) {
        boolean flag = false;
        try {
            ele.sendKeys(value);
            flag =true;
            return true;
        } catch (Exception e) {
            return false;
        }finally {
            if(flag) {
                System.out.println("Select value from the DropDown");
            }else {
                System.out.println("Not Selected value from the DropDown");
            }
        }

    }

    public static boolean selectByIndex(WebElement element,int index) {
        boolean flag = false;
        try {
            Select s = new Select(element);
            s.selectByIndex(index);
            flag = true;
            return true;
        } catch (Exception e) {
            return false;
        }finally {
            if(flag) {
                System.out.println("Option selected by Index");
            }else {
                System.out.println("Option not selected by Index");
            }
        }
    }

    public static boolean selectByValue(WebElement element,String value) {
        boolean flag = false;
        try {
            Select s = new Select(element);
            s.selectByValue(value);
            flag = true;
            return true;
        } catch (Exception e) {
            return false;
        }finally {
            if(flag) {
                System.out.println("Option selected by Value");
            }else {
                System.out.println("Option not selected by Value");
            }
        }
    }

    public static boolean selectByVisibleText(String visibletext,WebElement ele) {
        boolean flag = false;
        try {
            Select s = new Select(ele);
            s.selectByVisibleText(visibletext);;
            flag = true;
            return true;
        } catch (Exception e) {
            return false;
        }finally {
            if(flag) {
                System.out.println("Option selected by VisibleText");
            }else {
                System.out.println("Option not selected by VisibleText");
            }
        }
    }

    public static boolean mouseHoverByJavaScript(WebElement locator) {
        boolean flag = false;
        try {
            WebElement mo = locator;

            String javaScript = "var evObj = document.createEvent('MouseEvents');"
                    + "evObj.initMouseEvent('mouseover', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);"
                    + "arguments[0].dispatchEvent(evObj);";
            JavascriptExecutor js = (JavascriptExecutor)driver;
            js.executeScript(javaScript, mo);
            flag = true;
            return true;


        } catch (Exception e) {
            return false;
        }finally {
            if(flag) {
                System.out.println("MouseHover action is performed");
            }else {
                System.out.println("MouseHover action is not performed");
            }
        }
    }

    public static boolean JSClick(WebDriver driver,WebElement ele) throws Exception {
        boolean flag = false;
        try {
            JavascriptExecutor executor = (JavascriptExecutor)driver;
            executor.executeScript("arguments[0].click();", ele);
            flag = true;
            //return true;
        } catch (Exception e) {
            throw e;
        }finally {
            if(flag) {
                System.out.println("JS Click action is performed");
            }else if(!flag) {
                System.out.println("JS click action is not performed");
            }
        }
        return flag;
    }

    /*
     * public static boolean switchToFrameByIndex(int index,WebDriver driver) {
     * boolean flag = false; try { new WebDriverWait(driver,
     * 15).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(null));
     *
     * // If you reached here without exceptions, set the flag to true flag = true;
     * } catch (Exception e) { // Handle exception e.printStackTrace(); } return
     * flag; }
     */

    public static void mouseOverElement(WebElement element) {
        boolean flag = false;
        try {
            new Actions(driver).moveToElement(element).build().perform();
            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(flag) {
                System.out.println("MouseOver Action is Performed");
            }else {
                System.out.println("MouseOver Action is not Performed");
            }
        }
    }

    public static boolean moveToElement(WebDriver driver,WebElement ele) {
        boolean flag = false;
        try {
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].scrollIntoView(true);", ele);
            Actions actions = new Actions(driver);
            actions.moveToElement(ele).build().perform();
            flag = true;
        }catch(Exception e) {
            e.printStackTrace();
        }finally {
            if(flag) {
                System.out.println("Move to element is Performed");
            }else {
                System.out.println("Move to element is not Performed");
            }
        }
        return flag;
    }

}
