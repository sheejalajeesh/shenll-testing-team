package utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import io.github.bonigarcia.wdm.WebDriverManager;
import listeners.ListenerClassGrocery;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;
import reusableActionDriver.Actiondriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Properties;

public class GroceryUtils  {
    public static Properties prop;
    public static WebDriver driver;
    public static ExtentReports extent;
    private ExtentTest test;


    public static Properties  loadConfig() {

        try {
            prop = new Properties();
            FileInputStream ip = new FileInputStream(
                    System.getProperty("user.dir") + "\\Configuration\\config.properties");
            prop.load(ip);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop;
    }
    public WebDriver getDriver() {
        return driver;
    }
    @BeforeSuite
        public static void launchApp() {
        loadConfig();
            String browserName = prop.getProperty("browser");

           if(browserName.contains("chrome")) {
                WebDriverManager.chromedriver ().setup();
                driver = new ChromeDriver();

            } else if (browserName.contains("firefox")) {
                WebDriverManager.firefoxdriver ().setup();
                driver = new FirefoxDriver();

            } else if (browserName.contains("Edge")) {
               WebDriverManager.edgedriver().setup();
               driver = new EdgeDriver();
           }
        System.out.println(System.getProperty("user.dir") + "\\Configuration\\config.properties");
            Actiondriver.setImplicitWait(driver, 20);
            Actiondriver.setPageLoadTimeout(driver, 30);
            driver.manage().window().maximize();
            driver.get(prop.getProperty("url"));

        }

    @BeforeTest
    public void setup() {
        extent = new ExtentReports();
        ExtentSparkReporter spark = new ExtentSparkReporter("ExtentReport.html");
        extent.attachReporter(spark);
        test = extent.createTest("Grocery Go App Test", "Checking and Verifying the functionalities of Grocery Go Application")
                .assignAuthor("Tester").assignCategory("Grocery Go Test").assignDevice("Chrome");
    }

    @BeforeClass
    public void beforeMethod(ITestContext context) {
        test = extent.createTest(context.getName());
        context.setAttribute("extentTest", test);
    }

    public static void logExtentReport(ExtentTest test, Status status, String message) {
        test.log(status, message);
    }
    public void logTestStep(ExtentTest test, String description, Runnable testStep) {
        try {
            testStep.run();
            test.log(Status.PASS, description);
        } catch (Exception e) {
            test.log(Status.FAIL, description);
        }
    }
    @AfterMethod
    public void afterMethod(ITestResult result) {
        if (result.getStatus() == ITestResult.FAILURE) {
            logTestStep(test, "Test Failed", () -> {
                try {
                    TakesScreenshot ts = (TakesScreenshot) getDriver();
                    byte[] screenshot = ts.getScreenshotAs(OutputType.BYTES);
                    String screenshotFileName = "screenshot.png";
                    File screenshotFile = new File(screenshotFileName);
                    FileUtils.writeByteArrayToFile(screenshotFile, screenshot);
                    test.fail("Test Failed - Screenshot:",
                            MediaEntityBuilder.createScreenCaptureFromPath(screenshotFileName).build());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } else if (result.getStatus() == ITestResult.SUCCESS) {
            test.pass("Test Passed");
        }
        extent.flush();
    }
    @AfterTest
    public void teardown() {
        extent.flush();
        tearDown();

    }
    @AfterSuite
    public void tearDown(){
        driver.quit();
    }
    }


