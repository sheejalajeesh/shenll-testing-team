package locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CheckoutLocators {
    @FindBy(xpath = "//span[text()='Change']")
    public static WebElement Changeaddress;
    @FindBy (id="mat-radio-7-input")
    public static WebElement selectradio;
    @FindBy (xpath="//mat-icon[text()='close']")
    public static WebElement closebutton;
    @FindBy (xpath="(//img[@style='cursor: pointer;'])[4]")
    public static WebElement applycoupon;

   @FindBy(xpath="(//span[text()='APPLY'])[1]")
    public static WebElement apply;
@FindBy(xpath="//button[text()='Proceed to checkout']")
public static WebElement checkout;

    }


