package locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignUpLocators {

@FindBy(xpath="//button[text()='My Account']")
    public static WebElement myaccount;

    @FindBy(xpath="//span[@class='text-decoration-underline']")
    public static WebElement signuplink;

    @FindBy(name="name")
    public static WebElement name;




    

    @FindBy(id="exampleInputPhone")
    public static WebElement mobilenumber ;

    @FindBy(id="exampleInputEmail1")
    public static WebElement mailid ;
    @FindBy(xpath="//button[text()='Sign up']")
    public static WebElement signupbutton;
   @FindBy(xpath="(//input[@autocomplete='one-time-code'])[1]")
   public static WebElement otp1;

    @FindBy(xpath="(//input[@autocomplete='one-time-code'])[1]")
    public static WebElement otp2;
    @FindBy(xpath="(//input[@autocomplete='one-time-code'])[1]")
    public static WebElement otp3;
    @FindBy(xpath="(//input[@autocomplete='one-time-code'])[1]")
    public static WebElement otp4;

@FindBy(xpath="//button[text()='Verify']")
public static WebElement Verify;
@FindBy(xpath="//div[@class='signup__close-icon']")
    public static WebElement Closebutton;
@FindBy(xpath="//input[@placeholder='Enter your mobile number']")
    public static WebElement Loginmobilenumber;
    //@FindBy(xpath="//button[text()='Verify']")
    //public static WebElement Verify;

    @FindBy(xpath = "//button[@class='signup__button']")
    public static WebElement Verify1;
}
