package locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AddtoCartLocators {


    @FindBy(xpath="(//button[text()='Add to cart'])[1]")
    public static WebElement Addtocart1;
    @FindBy(xpath="(//button[text()='Add to cart'])[2]")
    public static WebElement Addtocart2;
    @FindBy(xpath="(//button[text()='Add to cart'])[3]")
    public static WebElement Addtocart3;
    @FindBy(xpath="\"(//button[text()='Add to cart'])[3]")
    public static WebElement Addtocart4;
    @FindBy(xpath="(//button[@type='button'])[2]")
    public static WebElement carticon;

    @FindBy(xpath="//span[text()=' + Add More ']")
    public static WebElement addmore;

    @FindBy(xpath="//div[text()=' Fruits & Vegetables']")
    public static WebElement fruitsveg;

    @FindBy(xpath="(//img[@class='mx-auto d-block w-50 cursor'])[1]")
    public static WebElement productdetailpage;
    @FindBy(xpath="(//img[@class='image-border img-fluid'])[2]")
    public static WebElement detailphoto;
    @FindBy(xpath="//button[text()=' Buy now ']")
    public static WebElement BuyNOW;


  @FindBy(xpath="(//span[text()='+'])[5]")
  public static WebElement increment;
    @FindBy(xpath="(//span[text()='+'])[5]")
    public static WebElement increment2;

    @FindBy(xpath="(//span[text()='-'])[4]")
    public static WebElement decrement1;
    @FindBy(xpath = "(//mat-icon[text()='cancel'])[1]")
    public static WebElement cancel;
    @FindBy(xpath="//span[text()='Yes']")
    public static WebElement Yes;
   @FindBy(xpath="(//img[@class='mx-auto d-block w-75'])[3]")
   public static WebElement category;
}
