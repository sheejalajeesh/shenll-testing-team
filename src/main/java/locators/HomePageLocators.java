package locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.GroceryUtils;

public class HomePageLocators extends GroceryUtils {

    @FindBy(xpath = "(//input[@placeholder='Search all products'])[1]")
    public static WebElement Search_Box;

    //Product
    @FindBy(xpath = "//span[text()='Sweet potato']")
    public static WebElement Select_Product;

    //After Search Selecting the product listed.

    //Click on GroceryGo to navigate to Home page
    @FindBy(xpath = "//img[@class='image_size ps-3']")
    public static WebElement Grocerygo_Click;

    //Select product from below listed Categories
    @FindBy(xpath = "//div[text()='CleaningHouseholds']")
    public static WebElement CleaningandHousehold_Click;

    //Select Store
    @FindBy(xpath = "(//div[@class='storeImage'])[2]")
    public static WebElement store_Name;

    //Sort By
    @FindBy(xpath = "//div[@id='mat-select-value-1']")
    public static WebElement sort_By;

    //Low to Price
    @FindBy(xpath = "//span[text()='Price : Low to High']")
    public static WebElement lowToHigh;

    //Home
    @FindBy(xpath="//span[text()='Home']")
    public static WebElement home;


}
