package locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginLocators {
    //Login Details
    @FindBy(xpath="//button[text()='My Account']")
    public static WebElement myaccount;

    @FindBy(xpath="//input[@placeholder='Enter your mobile number']")
    public static WebElement EnterMobNo;

    @FindBy(xpath = "//button[text()='Login']")
    public static WebElement login;

    //OTP
    @FindBy(xpath="(//input[@autocomplete='one-time-code'])[1]")
    public static WebElement otp1;

    @FindBy(xpath="(//input[@autocomplete='one-time-code'])[1]")
    public static WebElement otp2;
    @FindBy(xpath="(//input[@autocomplete='one-time-code'])[1]")
    public static WebElement otp3;
    @FindBy(xpath="(//input[@autocomplete='one-time-code'])[1]")
    public static WebElement otp4;

    //Verify
    @FindBy(xpath = "//button[text()='Verify']")
    public static WebElement verify;



}
