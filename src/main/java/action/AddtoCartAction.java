package action;

import locators.AddtoCartLocators;
import locators.SignUpLocators;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import reusableActionDriver.Actiondriver;
import utils.GroceryUtils;

public class AddtoCartAction extends GroceryUtils{
public void add() {
    PageFactory.initElements ( driver, AddtoCartLocators.class );
    Actiondriver.scrollDown(driver, 20);
    Actiondriver.setExplicitWait(driver,100);
    AddtoCartLocators.category.click ();
    Actiondriver.setExplicitWait(driver,20);
    AddtoCartLocators.Addtocart1.click ();
    Actiondriver.setExplicitWait(driver,20);
    AddtoCartLocators.Addtocart2.click ();
    Actiondriver.setExplicitWait(driver,20);
    AddtoCartLocators.Addtocart3.click ();
    Actiondriver.setExplicitWait(driver,20);
    AddtoCartLocators.Addtocart4.click ();
    Actiondriver.setExplicitWait(driver,20);
    AddtoCartLocators.carticon.click ();
    Actiondriver.setExplicitWait(driver,20);
    AddtoCartLocators.addmore.click ();
    Actiondriver.setExplicitWait(driver,20);
    AddtoCartLocators.fruitsveg.click ();
    Actiondriver.setExplicitWait(driver,20);
    AddtoCartLocators.productdetailpage.click ();
    Actiondriver.setExplicitWait(driver,20);
    AddtoCartLocators.detailphoto.click ();
}
@Test
    public void cartpage() {
    add();
    Actiondriver.setExplicitWait(driver,20);
        AddtoCartLocators.BuyNOW.click ();
    Actiondriver.setExplicitWait(driver,20);
        AddtoCartLocators.increment.click ();
    Actiondriver.setExplicitWait(driver,20);
        AddtoCartLocators.increment2.click ();
    Actiondriver.setExplicitWait(driver,20);
        AddtoCartLocators.decrement1.click ();
    Actiondriver.setExplicitWait(driver,20);
        AddtoCartLocators.cancel.click ();
    Actiondriver.setExplicitWait(driver,20);
        AddtoCartLocators.Yes.click ();

    Actiondriver.setExplicitWait(driver,20);
        AddtoCartLocators.Yes.click ();

    }







}
