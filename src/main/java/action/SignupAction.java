package action;

import locators.SignUpLocators;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import reusableActionDriver.Actiondriver;
import utils.GroceryUtils;

import java.text.NumberFormat;


public class SignupAction extends GroceryUtils {

    public void signup() throws InterruptedException {
        PageFactory.initElements ( driver, SignUpLocators.class );
        Thread.sleep ( 5000 );
        SignUpLocators.myaccount.click ();
        Thread.sleep ( 1000 );

        SignUpLocators.signuplink.click ();
        Thread.sleep ( 1000 );
        SignUpLocators.name.sendKeys ( prop.getProperty ( "name" ) );
        SignUpLocators.mobilenumber.sendKeys ( prop.getProperty ( "PhNo" ) );
        SignUpLocators.mailid.sendKeys ( prop.getProperty ( "mail" ) );
        Thread.sleep ( 1000 );
        Thread.sleep ( 5000 );
        SignUpLocators.signupbutton.click ();
        System.out.println ( "Test" );
    }}
  /*  @Test
        public void otp() throws InterruptedException {
           signup ();
           PageFactory.initElements ( driver, SignUpLocators.class );
           Thread.sleep ( 5000 );
           SignUpLocators.otp1.click ();
            SignUpLocators.otp2.click ();
            SignUpLocators.otp3.click ();
            SignUpLocators.otp4.click ();
            SignUpLocators.Verify.click ();
        }

}*/
