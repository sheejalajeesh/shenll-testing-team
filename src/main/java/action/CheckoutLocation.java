package action;

import locators.CheckoutLocators;
import locators.HomePageLocators;
import org.openqa.selenium.support.PageFactory;
import reusableActionDriver.Actiondriver;
import utils.GroceryUtils;

public class CheckoutLocation extends GroceryUtils {
    public void checkout(){
        PageFactory.initElements(driver, HomePageLocators.class);
        Actiondriver.setExplicitWait(driver,40);
        CheckoutLocators.Changeaddress.click ();
        Actiondriver.setExplicitWait(driver,40);
        CheckoutLocators.selectradio.click ();
        Actiondriver.setExplicitWait(driver,40);
                CheckoutLocators.closebutton.click ();
        Actiondriver.setExplicitWait(driver,40);
        CheckoutLocators.applycoupon.click ();
        Actiondriver.setExplicitWait(driver,40);
        CheckoutLocators.apply.click ();
        Actiondriver.setExplicitWait(driver,100);
                CheckoutLocators.checkout.click ();

    }

}
