package action;

import locators.LoginLocators;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import utils.GroceryUtils;

public class LoginAction extends GroceryUtils {

    public void logIn() throws InterruptedException {
        PageFactory.initElements ( driver, LoginLocators.class );
        Thread.sleep ( 5000 );
        LoginLocators.myaccount.click ();
        Thread.sleep ( 1000 );
        LoginLocators.EnterMobNo.sendKeys ( prop.getProperty ( "MobNo" ) );
        Thread.sleep ( 5000 );
        LoginLocators.login.click();
    }
    @Test
    public void otpVerify() throws InterruptedException {
        logIn ();
        PageFactory.initElements ( driver, LoginLocators.class );
        Thread.sleep ( 5000 );
        LoginLocators.otp1.click ();
        LoginLocators.otp1.sendKeys(prop.getProperty("otp"));
        LoginLocators.verify.click ();
    }

}

