package action;

import locators.HomePageLocators;
import locators.SignUpLocators;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import reusableActionDriver.Actiondriver;
import utils.GroceryUtils;

  public class HomePageAction extends GroceryUtils {
@Test
 public void search_Home_Products() throws InterruptedException {
        PageFactory.initElements(driver, HomePageLocators.class);
        Actiondriver.setExplicitWait(driver,500);
        HomePageLocators.Grocerygo_Click.click();
        Actiondriver.setExplicitWait(driver,200);
        HomePageLocators.Search_Box.click();
        Actiondriver.setExplicitWait(driver,40);
        HomePageLocators.Search_Box.sendKeys("Potato");
        Actiondriver.setExplicitWait(driver,20);
        HomePageLocators.Select_Product.click();
        Actiondriver.setExplicitWait(driver,40);
        HomePageLocators.Grocerygo_Click.click();
        Actiondriver.setExplicitWait(driver,60);
        Actiondriver.setExplicitWait(driver,40);
        Actiondriver.scrollDown(driver, 120);
        Actiondriver.setExplicitWait(driver,200);
        HomePageLocators.CleaningandHousehold_Click.click();
        Actiondriver.setExplicitWait(driver,400);

        HomePageLocators.store_Name.click();

        Actiondriver.setExplicitWait(driver,100);
        HomePageLocators.sort_By.click();
        Actiondriver.setExplicitWait(driver,40);
        HomePageLocators.lowToHigh.click();
        Actiondriver.setExplicitWait(driver,40);
        HomePageLocators.home.click();
    }


}
